---
Title: webscarab
Homepage: http://dawes.za.net/rogan/webscarab/#current
Repository: https://gitlab.com/kalilinux/packages/webscarab
Architectures: all
Version: 20120422-001828-1kali2
Metapackages: kali-linux-everything kali-linux-large kali-tools-web 
Icon: images/webscarab-logo.svg
PackagesInfo: |
 ### webscarab
 
  WebScarab is designed to be a tool for anyone who needs to
  expose the workings of an HTTP(S) based application,
  whether to allow the developer to debug otherwise difficult
  problems, or to allow a security specialist to identify
  vulnerabilities in the way that the application has been
  designed or implemented.
 
 **Installed size:** `12.41 MB`  
 **How to install:** `sudo apt install webscarab`  
 
 {{< spoiler "Dependencies:" >}}
 * default-jre
 {{< /spoiler >}}
 
 ##### webscarab
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## Screenshots

```
webscarab
```

![webscarab](images/webscarab.png)
